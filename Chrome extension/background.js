	// Copyright 2018 The Chromium Authors. All rights reserved.
	// Use of this source code is governed by a BSD-style license that can be
	// found in the LICENSE file.

	'use strict';

	var callForAO = function() {
		
		chrome.tabs.create({
			"url": "https://anmup.online"
		}, function (tab) {

		});
	}

	chrome.runtime.onInstalled.addListener(function() {
	  	// Syncing the storage
		chrome.storage.sync.set({color: '#3aa757'}, function() {
			console.log("Green is the Color.");
		});
		// Host
		chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {

			chrome.declarativeContent.onPageChanged.addRules(
				[
					{ 
						conditions: [
		  				new chrome.declarativeContent.PageStateMatcher
			  				({ 
			  					pageUrl: {} 
		  					})
						], 
						actions: [
							new chrome.declarativeContent.ShowPageAction()
						] 
					}
				]
				);
		});

		chrome.commands.onCommand.addListener(function(command) {
			if(command==="toggle-feature") {
				callForAO();
			}
		});
	});	
